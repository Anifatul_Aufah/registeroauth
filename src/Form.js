import React, { Component } from "react";
import "./Form.css";
import firebase from 'firebase';
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";

const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const formValid = ({ formErrors, ...rest }) => {
  let valid = true;
  // validasi form error akan kosong
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });
  //validasi 
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};

firebase.initializeApp({
  apiKey:"AIzaSyATa8Eiy0G653ziI2GdAVUAkajOkBKFKFI",
  authDomain:"fir-auth-react-77383.firebaseapp.com"
});

class Form extends Component {
  state = { isSignedIn : false }
    uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccess: () => false
    }
  };

  componentDidMount = () => {
    firebase.auth().onAuthStateChanged(user => {
      this.setState({isSignedIn:!!user})
    })
  };  

  constructor(props) {
    super(props); 

    this.state = {
      userName: null,
      firstName: null,
      lastName: null,
      email: null,
      emailConfirm: null,
      phoneNumber: null,
      password:null,
      password2: null,
      formErrors: {
        userName: "",
        firstName: "",
        lastName: "",
        email: "",
        emailConfirm: "",
        phoneNumber: "",
        password: "",
        password2: ""
      }
    };
  
  //   this.handleSubmit = this.handleSubmit.bind(this);
  }
   handleSubmit = e => {
     e.preventDefault();

     if (formValid(this.state)) {
      console.log(`
        --SUBMITTING--
        User Name : ${this.state.userName}
        First Name : ${this.state.firstName}
        Last Name : ${this.state.lastName}
        Email : ${this.state.email}
        Confirm Email : ${this.state.emailConfirm}
        Phone Number : ${this.state.phoneNumber}
        Password : ${this.state.password}
        Confirm Password : ${this.state.password2}
        `);
      } else {
        console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
     }
   };

   handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };

    switch (name) {
      case "userName":
        formErrors.userName = value.length < 3 ? "minimum 3 character required" 
         :""; 
      break;
      case "firstName":
        formErrors.firstName = value.length < 3 ? "minimum 3 character required"
         :""; 
      break;
      case "lastName":
        formErrors.lastName = value.length < 3 ? "minimum 3 character required"
         :""; 
      break;
      case "email":
        formErrors.email = emailRegex.test(value) ? ""
         :"invalid email address"; 
      break;
      case "emailConfirm":
        formErrors.emailConfirm = emailRegex.test(value) ? ""
         :"invalid email address"; 
      break;
      case "phoneNumber":
        formErrors.phoneNumber = value.length < 6
         ? "minimum 11 character required"
         :""; 
      break;
      case "password":
        formErrors.password = value.length < 6
         ? "minimum 6 character required"
         :""; 
      break;
      case "password2":
        formErrors.password2 = value.length < 6 
         ? "minimum 6 character required"
         :""; 
      break;
      default:
      break;
    }
    this.setState({ formErrors, [name]:value }, () => console.log(this.state));  
   }; 

  render() {
    const {formErrors} = this.state;

    return (
      <div id="login-box">
        <div className="row">

        <div className="left">
          <h1>Sign up</h1>
          <p> Belum punya Akun? Isi dan submit form dibawah ini dan Selamat Bergabung!</p>
          
          <div className="userName">
          <i className="fa fa-user icon"></i>
                <label htmlFor="userName">User Name</label>
                <input 
                  type="text"
                  className={formErrors.userName.length > 0 ? "error" : null}
                  placeholder="Input User Name"
                  type="text"
                  name="userName"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.userName.length > 0 && (
                  <span className="errorMessage">{formErrors.userName}</span>
                )}
          </div>
          <div className="firstName">
                <label htmlFor="firstName">First Name</label>
                <input 
                  type="text"
                  className={formErrors.firstName.length > 0 ? "error" : null}
                  placeholder="Input First Name"
                  type="text"
                  name="firstName"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.firstName.length > 0 && (
                  <span className="errorMessage">{formErrors.firstName}</span>
                )}
          </div>
          <div className="lastName">
                <label htmlFor="lastName">Last Name</label>
                <input
                  type="text"
                  className={formErrors.lastName.length > 0 ? "error" : null}
                  placeholder="Input Last Name"
                  type="text"
                  name="lastName"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.lastName.length > 0 && (
                  <span className="errorMessage">{formErrors.lastName}</span>
                )}
          </div>
          <div className="email">
                <label htmlFor="email">Email</label>
                <input 
                  type="email"
                  className={formErrors.email.length > 0 ? "error" : null}
                  placeholder="Input Email"
                  type="text"
                  name="email"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.lastName.length > 0 && (
                  <span className="errorMessage">{formErrors.email}</span>
                )}
          </div>
          <div className="emailConfirm">
                <label htmlFor="emailConfirm">Confirm Email</label>
                <input 
                  type="email"
                  className={formErrors.emailConfirm.length > 0 ? "error" : null}
                  placeholder="Input Email Again"
                  type="text"
                  name="emailConfirm"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.lastName.length > 0 && (
                  <span className="errorMessage">{formErrors.emailConfirm}</span>
                )}
          </div>
          <div className="phoneNumber">
                <label htmlFor="phoneNumber">Phone Number</label>
                <input
                  type="text"
                  className={formErrors.phoneNumber.length > 0 ? "error" : null}
                  placeholder="Input Phone Number"
                  type="text"
                  name="phoneNumber"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.lastName.length > 0 && (
                  <span className="errorMessage">{formErrors.phoneNumber}</span>
                )}

          </div>
          <div className="password">
                <label htmlFor="password">Password</label>
                <input 
                  type="text"
                  className={formErrors.password.length > 0 ? "error" : null}
                  placeholder="Input Password"
                  type="password"
                  name="password"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.lastName.length > 0 && (
                  <span className="errorMessage">{formErrors.password}</span>
                )}
          </div>
          <div className="password2">
                <label htmlFor="password">Confirm Password</label>
                <input 
                  type="text"
                  className={formErrors.password2.length > 0 ? "error" : null}
                  placeholder="Input Password Again"
                  type="password"
                  name="password2"
                  noValidate
                  onChange={this.handleChange}
                />
                {formErrors.lastName.length > 0 && (
                  <span className="errorMessage">{formErrors.password2}</span>
                )}
          </div>
          <button type="submit" className="submit">Sign Up</button>
        </div>
        </div>
        
        <div className="right">
          <span className="loginwith">Sign in with<br />social network</span>
          
          {this.state.isSignedIn ? (
          <span>
          <div>Signed In!</div>
          <button onClick={() => firebase.auth().signOut()}>Sign out!</button>
          </span>
          ): (
          <StyledFirebaseAuth
          uiConfig={this.uiConfig}
          firebaseAuth={firebase.auth()}
          />  
          )}
        </div>

        <div className="or">OR</div>
      </div>
    );
  }
}
export default Form;